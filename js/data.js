/** initialize jQuery **/
$.ajaxSetup({
	async: false
});



/** load the language file and insert the strings to the page **/
var language;
$.getJSON("lang/german.json", function(json) {
	language = json;

	getElement('channelName').innerHTML = language.player.choose;
	getElement('channelGenreDescription').innerHTML = language.player.genre + ':';
	getElement('channelBitrateDescription').innerHTML = language.player.bitrate + ':';
	getElement('channelBitrateUnit').innerHTML = language.player.bitrateUnit;
	getElement('searchbar').placeholder = language.search.searchbar;
	getElement('channellistHeadline').innerHTML = language.list.headline;
	getElement('mixedContentHint').innerHTML = language.hint.mixedContent;
});




/** load the channel list into a variable **/
$.getJSON("data/data.json", function(json) {
	window.channelJSON = json;
});

channelJSON.sort(function(a, b) {
	var x = a.name.toLowerCase();
	var y = b.name.toLowerCase();
	if (x < y) {return -1;}
	if (x > y) {return 1;}
	return 0;
});



/** function for loading the channels **/
function getChannels(searchstring) {
	var json = window.channelJSON;
	var list = getElement('channellist');
	var message = getElement('channellistMessage');

	var channels = '';
	var print;
	message.innerHTML = '';

	for (channelID in json) {
		data = json[channelID];
		print = false;

		if (data.name.toLowerCase().includes(searchstring.toLowerCase())) {
			print = true;
		}

		if (searchstring == '' || print) {
			channels += '<li><a href="#" id="channel_'+ channelID +'" channelID="channel_'+ channelID +'" channelName="'+ data.name +'" channelBitrate="'+ data.kbits +'" channelGenre="'+ data.genre +'" channelURL="'+ data.url +'">'+ data.name +'</a></li>';
		}
	}

	if (channels == '') {
		message.innerHTML = language.list.empty;
	}
	list.innerHTML = channels;


	/** channel list entries --> click to play channel **/
	$("[channelID]").on("click", function() {
		var url = this.attributes.channelURL.value;
		var name = this.attributes.channelName.value;
		var genre = this.attributes.channelGenre.value;
		var bitrate = this.attributes.channelBitrate.value;

		playStream(getPlayer(), getElement('StopPlayIcon'), url);
		updatePlayerInfo(name, genre, bitrate);
	});
}


/** initial loading if all channels **/
getChannels('');

