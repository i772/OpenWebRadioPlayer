$(document).ready(function() {
	/** play/stop button **/
	$("#StopPlayButton").on("click", function() {
		/** get the icon and the player as an variable **/
		var icon = getElement('StopPlayIcon');
		var player = getPlayer();

		if (icon.attributes.src.value == "img/stop.png") {
			player.pause();
			player.src = '';
			icon.src = "img/play.png";
			updatePlayerInfo('', '', '');
			writeLog('Stopping stream!');
		}
	});



	/** mute button **/
	$("#muteButton").on("click", function() {
		/** get the icon and the player as an variable **/
		var icon = getElement('muteIcon');
		var player = getPlayer();

		/** if theres the unmuted icon and we click it, we will change to the muted icon and mute the player ( **/
		if (icon.attributes.src.value == "img/speaker.png") {
			player.muted = true;
			icon.src = "img/mutespeaker.png";
			writeLog('Muted!');
		} else {
			player.muted = false;
			icon.src = "img/speaker.png";
			writeLog('Unmuted!');
		}
	});



	/** volume scale **/
	$("#volumescale").on("input", function() {
		var display = document.getElementById('volumedisplay');
        	var player = getPlayer();

		var volume = this.value;

		if (volume < 10) {
			display.innerHTML = '&ensp;'+ (volume * 10) +'%';
		} else {
			display.innerHTML = (volume * 10) +'%';
		}

		player.volume = volume / 10;

		writeLog("Volume was set to "+ volume * 10 +'%');
	});



	/** reloading and searching in channels if theres input in the searchbar **/
	$("#searchbar").on("input", function() {
		var searchstring = this.value;
		getChannels(searchstring);
	});



	/** clear the search bar and reload the channel list **/
	$("#clearSearchbar").click(function() {
		var searchbar = getElement('searchbar');
		searchbar.value = '';
		getChannels('');
	});
});
