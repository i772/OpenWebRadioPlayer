/** function for fast getting a element **/
function getElement(id) {
	return document.getElementById(id);
}

/** function for selecting the player element **/
function getPlayer() {
	return getElement('radioplayer');
}

/** function for writing a message to the browser log/console **/
function writeLog(msg) {
	console.log('[OpenWebRadioPlayer] '+ msg);
}

/** function for loading and playing a stream **/
function playStream(player, icon, url) {
	icon.src = "img/stop.png";
	player.pause();
	player.src = url;
	try {
		player.load();
		player.play();
	} catch (e) {
		alert("Error playing stream!");
		console.log(e);
	}
	writeLog('Playing now '+ url);
}

/** function for updating the channel informations in the upper box **/
function updatePlayerInfo(name, genre, bitrate) {

	var channelName = getElement('channelName');
	var siteTitle = getElement('siteTitle');
	if (name == '') {
		channelName.innerHTML = language.player.choose;
		siteTitle.innerHTML = 'OpenWebRadioPlayer';
	} else {
		channelName.innerHTML = language.player.listento + name;
		siteTitle.innerHTML = name +' - OpenWebRadioPlayer';
	}

	var channelGenre = getElement('channelGenre');
	if (genre == '') {
		channelGenre.innerHTML = '-';
	} else {
		channelGenre.innerHTML = genre;
	}

	var channelBitrate = getElement('channelBitrate');
	if (bitrate == '') {
		channelBitrate.innerHTML = '-';
	} else {
		channelBitrate.innerHTML = bitrate;
	}
}
