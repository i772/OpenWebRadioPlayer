## OpenWebRadioPlayer

This is a simple open-source WebRadioPlayer that needs no backend services.  

The streaming URLs are stored in `data/data.json`. Feel free to commit more or report broken streams!  

In the `lang` directory you will find some JSON language files - just insert the filename of the language that you want to use in `js/data.js` and move on in your language. Feel free to contribute more languages!  

### Next steps:  
- check https capability of streaming links  
- add country property in data file and implement it in player  